package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.entity.Vo;
import com.kgc.cn.provider.model.Dto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wangwei
 * @date 2020/2/17 - 14:51
 */
public interface TestMapper {
    List<Dto> queryData(@Param("dto") Dto dto);
}
