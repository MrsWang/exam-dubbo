package com.kgc.cn.provider.model;

import lombok.Data;

/**
 * @author wangwei
 * @date 2020/2/17 - 15:11
 */
@Data
public class Dto {
    private String name;
    private int age;
    private String id;
}
