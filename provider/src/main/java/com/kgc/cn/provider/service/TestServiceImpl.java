package com.kgc.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.entity.Vo;
import com.kgc.cn.common.service.TestService;
import com.kgc.cn.provider.mapper.TestMapper;
import com.kgc.cn.provider.model.Dto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wangwei
 * @date 2020/2/17 - 14:43
 */
@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestMapper testMapper;

    @Override
    public String queryData(Vo vo) {
        Dto dto = new Dto();
        BeanUtils.copyProperties(vo,dto);
        return testMapper.queryData(dto).toString();
    }

}
