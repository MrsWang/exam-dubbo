package com.kgc.cn.consumer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangwei
 * @date 2020/2/17 - 14:15
 */
@Data
@ApiModel("人")
public class Param {
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("年龄")
    private int age;
}
