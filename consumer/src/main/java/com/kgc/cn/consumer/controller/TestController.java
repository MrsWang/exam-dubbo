package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.entity.Vo;
import com.kgc.cn.common.service.TestService;
import com.kgc.cn.consumer.param.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author wangwei
 * @date 2020/2/17 - 13:52
 */
@RestController
@Api(tags = "测试")
@RequestMapping(value = "/test")
public class TestController {
    @Reference
    private TestService testService;

    @ApiOperation("测试方法")
    @PostMapping(value = "/queryData")
    public String queryData(@Valid @ApiParam("年龄姓名")Param param){
        Vo vo = new Vo();
        BeanUtils.copyProperties(param,vo);
        return testService.queryData(vo);
    }

}
