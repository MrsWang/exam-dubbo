package com.kgc.cn.common.util.constant;


import java.io.Serializable;

/**
 * @author wangwei
 * @date 2019/12/7 - 0:07
 */
public class Param implements Serializable {
    // 订单版本号
    public static final String ORDER_VERSION = "108614";
    // 最大点击数
    public static final long MAX_CLICK = 10;
    // 设置最大点击时间
    public static final long CLICK_MOST_TIME = 60;
    // 支付超时时间
    public static final long PAY_TIMEOUT = 900;
    // redis加锁最大超时时间
    public static final long LOCK_TTL_TIME = 1;

}
