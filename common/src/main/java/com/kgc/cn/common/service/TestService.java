package com.kgc.cn.common.service;

import com.kgc.cn.common.entity.Vo;

/**
 * @author wangwei
 * @date 2020/2/17 - 12:19
 */
public interface TestService {
    String queryData(Vo vo);
}
