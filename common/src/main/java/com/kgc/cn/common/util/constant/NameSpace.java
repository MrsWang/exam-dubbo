package com.kgc.cn.common.util.constant;

import java.io.Serializable;

/**
 * @author wangwei
 * @date 2019/12/7 - 0:02
 */
public class NameSpace implements Serializable {

    public static final String GOODS_NUMS_NAMESPACE = "goodsNums:";
    public static final String CHECK_SPEED_NAMESPACE = "checkSpeed:";
    public static final String USER_LOGIN_NAMESPACE = "userLogin:";
    public static final String LOCK_ORDERS = "lockOrders:";
    public static final String LOCK_GOODS = "lockGoods:";
    public static final String ORDER_NAMESPACE = "order:";
    public static final String GOODS_NAMESPACE = "goods:";
    public static final String JURISDICTION = "jurisdiction:";
    public static final String ROLE_LOGIN_NAMESPACE = "roleLogin:";
    public static final String JOBNUM_NAMESPACE = "kh73";
    public static final String JOBNUM_CREATE = "createJobNumber";
    public static final String ADDROLEFROMEXCEL="addRoleFromExcel";
}
