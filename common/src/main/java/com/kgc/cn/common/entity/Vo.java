package com.kgc.cn.common.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wangwei
 * @date 2020/2/17 - 13:51
 */
@Data
public class Vo implements Serializable {
    private String name;
    private int age;
}
